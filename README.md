# E-Commerce Product Analysis in Python
The project is an exploratory data analysis accomplished on an assignment of an e-commerce company's product manager, implemented with Python.  

## Project Objective 
Analyze the purchases and answer the following questions:

1. [Number of users who made a purchase only once.](#Number_of_users_who_made_a_purchase_only_once)
2. [How many orders per month on average are not delivered for various causes? Output the details of the causes.](#How_many_orders_per_month)
3. [For each product, determine which day of the week the product is most frequently purchased.](#For_each_product)
4. [How many purchases does each user make on average per week (by month)?](#How_many_purchases)
5. [Conduct a cohort analysis of users. Identify the cohort with the highest retention for the 3rd month for the period from January to December.](#5_The_cohort_analysis_of_users)
6. [Build RFM segmentation of users with metrics:](#6_rfm_segmentation)
   * Recency (R) is how long it has been since the customer last purchased the product,
   * Frequency (F) is the frequency of customer purchases or total number of purchases made by the user over time,
   * Monetary (M) is the total amount the customer has paid since the first purchase.
  
## Stack

 * Python/libraries:
    * Pandas
    * numpy
    * seaborn
    * matplotlib

* EDA
* Cohort analysis
* RFM analysis

## Datasets 

 
I used 3 datasets for the analysis:
1. [The table with unique user IDs](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/olist_customers_dataset.csv?ref_type=heads) contains the following columns:

* customer_id — user ID by order 
* customer_unique_id —  unique user identifier 
* customer_zip_code_prefix —  user zip code 
* customer_city —  user's delivery city
* customer_state —  user's delivery state 

2.  [The Order table](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/olist_orders_dataset.csv?ref_type=heads) includes order data:

* order_id —  unique order ID (check number) 
* customer_id — user ID by order 
* order_status — The table has  Unique order statuses: 
  * created 
  * approved 
  * invoiced 
  * processing — in the process of order assembly
  * shipped 
  * delivered
  * unavailable 
  * canceled 
    
* order_purchase_timestamp —  order creation time 
* order_approved_at —  time of order payment confirmation 
* order_delivered_carrier_date — date and time of order transfer to the logistics service 
* order_delivered_customer_date — date and time of order delivery to the customer 
* order_estimated_delivery_date.

3. The table with information about product items included in orders: [olist_order_items_dataset.csv](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/olist_order_items_dataset.csv?ref_type=heads)  

* order_id  
* order_item_id — item ID within one order 
* product_id  
* seller_id 
* shipping_limit_date — maximum delivery date for the seller to transfer the order to the logistics partner 
* price — one item price 
* freight_value — item weight 

![An example of the data structure in the third dataset.](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/Data_structure_example.png?ref_type=heads) 

One line = one product. If a product is ordered in quantity, for example, 3 pieces, there will be 3 rows with the same order and product id.
All tables are in this repository.
 
## The results

For convenience, I've divided the project into 3 files/code notebooks:

* [The first file](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/Project_e-commerce_by_Natalie_Lebedeva(Swan).ipynb?ref_type=heads) has performed preliminary (Exploratory )data analysis (EDA)  and answers the first 4 questions.
* [The second file](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/User_cohort_analysis.ipynb?ref_type=heads) contains code for cohort analysis.

* [In the third file](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/rfm_segmentation.ipynb?ref_type=heads) You can check the RFM analysis.

## EDA
You can check the complete code [there](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/Project_e-commerce_by_Natalie_Lebedeva(Swan).ipynb?ref_type=heads).
 
I'll just put the main points here. 

All the dates have been converted to datetime format: 
* The 4th of September, 2016 is the first date when an order was formed.  
* The last date in the table is the 17th of October, 2018. It’s the date of order creation and delivery to the customer.
* The last estimated delivery date is the 11th of December, 2018.

So, I took October 17, 2018 as the current date.

Customers placed orders from 27 states and 4119 cities in Brazil. That country has 26 states and 1 district.

There are empty cells only in three columns of one of the 3 tables. And they look logical enough. Most likely, it means there were no certain operations with the orders, so the cells were not filled in.

Each order has a creation date and an estimated date of delivery. Apparently, 160 orders were not paid, 1783 were not delivered to the logistics company and 2965 were not delivered to the customer. Although I can't rule out the possibility of technical bugs.
 

![Number of empty cells in olist_order_items_dataset.csv](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/isna.png?ref_type=heads)

 
Overall the base is pretty clean, but there are a couple issues.

### Data issues
As far as I can understand, the sequence of operations is as follows: 
1. An order is created and given a status 'created',
2. Then the customer confirms the order -  'approved',
3. After that an invoice is issued - 'invoiced',
4. And only then the payment is made, we see the date of its confirmation, and the order goes to work with the status 'processing'.
  

There are only 2 orders with status approved in the data, both are paid. Why were these orders not delivered until December 11, 2018 (with the estimated delivery dates in 2017), even though the orders were confirmed, paid, and not canceled? Technically it turns out the invoice was paid before it was issued because a paid order should have a status of invoiced or processing at least. It looks like a bug.

![Undelivered paid orders with status 'approved'](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/approved.png?ref_type=heads)


There are also such anomalies: an order may have the status 'delivered', but it has no payment date (14 orders), no delivery date to the logistics company (2 orders), and even no delivery date to the customer (8 orders). But all these are single cases, looking like a bug.

![The bugs with status 'delivered'](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/delivered.png?ref_type=heads)

We should pay more attention to paid orders that should have been delivered (sometimes long) before the day the data had been uploaded to the file - October 17, 2018. However, they somehow hung up each one at its stages. But we will address this problem when answering the 2nd question of the project.  


## Key findings

### 1. Number of users who made a purchase only once <a name="Number_of_users_who_made_a_purchase_only_once"></a> 
I suggest to consider created and paid order as a purchase (an order has a date of payment confirmation). You can see a detailed study of this topic in the first notebook. 

To answer that question, I created a new dataframe by taking the 'order_approved_at' column from the orders table and merging it with customer_unique_id from the table with unique user IDs, while deleting empty cells = unpaid orders:

```python
paid_orders = orders[['customer_id', 'order_approved_at']]\
.merge(customers[['customer_id', 'customer_unique_id']], how='inner', on='customer_id')\
.dropna()
```
 
Then I calculated how many orders each user had and counted customers who only had 1 order:

```python
num_orders = pd.DataFrame(paid_orders.groupby('customer_unique_id', as_index=False)['customer_id'].count())
customers_with_1_purchase = num_orders.query('customer_id == 1')['customer_unique_id'].count()

print(customers_with_1_purchase, 'users made a purchase once')
```

### 93049 of 96096 unique users made a purchase once. 


### 2. How many orders per month on average are not delivered for various causes? Output the details of the causes <a name="How_many_orders_per_month"></a>

**What we would consider undelivered goods**

 
First, let's see if there are any orders delivered after the estimated date.

```python
delays = orders.query('order_delivered_customer_date > order_estimated_delivery_date')
delays.order_id.nunique()
```
7,827 orders were delivered late. By month, they can be considered undelivered. Accordingly, I will determine whether the order was delivered or not in the month for which the delivery date is scheduled (estimated).

```python
#  Filter out orders delivered after the estimated date and with empty cells in the column order_delivered_customer_date
undelivered_orders = orders\
.query('order_delivered_customer_date > order_estimated_delivery_date or order_delivered_customer_date.isna()')

#  create a new column to see how many days the delay of orders was
undelivered_orders['delay_days'] \
= (undelivered_orders.order_delivered_customer_date - undelivered_orders.order_estimated_delivery_date)/ np.timedelta64(1, 'D')
undelivered_orders.describe()
```

On average, the delivery delay was about 10 days. Half the orders were delivered no more than 6 days after the estimated date. 75% of delays did not exceed 12 days. But sometimes people waited half a year for their orders. 

![The delivery delay, days](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/boxplot_delay.png?ref_type=heads)

The question is what we would consider a delay and what we would consider an undelivered good. 
Let's say that we look at the data on the first day of each month. Some orders will be delivered on time the previous month, some will be delayed, and some will not have been delivered by the day the data is studied. They may be delivered later, but people can't know for sure what will happen in the following days. So we will consider orders that do not have a delivery mark in the previous month (with an estimated date in this month) as undelivered.    

** An order is considered undelivered if it is not delivered in the promised month**. 
Let's make a small “indulgence”: if the delay is not more than a day, the order will be considered delivered (in case an estimated delivery date is the last day of the month, and we analyze the data on the first day of the next month). 

```python
# Filter out orders that were delivered with a delay of more than 1 day 

undelivered_orders = undelivered_orders\
.query('delay_days >= 2 or order_delivered_customer_date.isna()') 

# Now only keep the orders that weren't delivered in the month of the estimated date:
undelivered_orders = undelivered_orders\
.query('(order_delivered_customer_date.dt.to_period("M") > estimated_delivery_year_month) or (order_delivered_customer_date.isna())') 
undelivered_orders.order_id.nunique()
```
**So we've got 4,938 undelivered orders.**  
 Let's group them by month and calculate the average

 ```python
undelivered_orders.groupby(['estimated_delivery_year_month']).agg({'order_id':'count'}).mean()
```

### About 190 orders per month on average are not delivered for various causes.
Let's see how the statuses are distributed among undelivered orders

```python
data = undelivered_orders.groupby('order_status', as_index=False)[['order_id']].count()

#merge the statuses 'approved' and 'created'  в 'other'
other = ["approved", "created"]
a = data.query('order_status in @other')['order_id'].agg("sum")
new_row = pd.Series({'order_status': 'other', 'order_id': a})
data_1 = pd.concat([data, pd.DataFrame([new_row])], ignore_index=True).sort_values(['order_id']).reset_index(drop=True)
data_1.drop(data_1.index[[0,1]], inplace=True)
data_1
```
![Table Statuses of undelivered orders](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/statuses_tab.png?ref_type=heads)

![Statuses of undelivered orders](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/output.png?ref_type=heads)

* 40% of orders were eventually delivered, but later than promised, not even in the promised month*.
*If the estimated date is at the end of the month and the delay is more than a day, the order is placed on the undeliverable list.

* 13% of orders were canceled
* 12% of orders have the status 'unavailable' 
* 22% of orders were delivered to the logistics company and are still there or lost.
* 12% of orders are still have statuses 'created', 'approved', 'invoiced' и 'processing'

If the last statuses are more or less clear, I don't understand what or who is unavailable. I found in the table ‘items’ only 6 orders (out of 609) with the status ‘unavailable’. Moreover, they have the delivery date to logistics, but for some reason, these items had not been shipped by a seller.
 
Where did the other 603 orders disappear to? Maybe the sellers who were selling the ordered goods became unavailable?

### 3. For each product, determine which day of the week the product is most frequently purchased <a name="For_each_product"></a>

We consider the purchase date as the date of order creation.

To solve this, let's take the order creation time from the orders table and merge it with the product id from the items table. From the order time, we will select the day of the week and group data by product_id and day of the week.

```python
ordered_items = orders[['order_id', 'order_purchase_timestamp']]\
.merge(items[['order_id', 'product_id']], how='left', on='order_id')
ordered_items['day_of_week'] = ordered_items.order_purchase_timestamp.dt.day_name()
orders_per_day = ordered_items.groupby(['product_id', 'day_of_week'], as_index=False)\
.agg(number_orders = ('order_id', 'count'))
orders_per_day.sort_values('number_orders', ascending=False)
```

Now we can substitute the ID of the item we are interested in into the code below and see its sales by day of the week:

```python
item = '422879e10f46682990de24d770e7f83d'
item_dow = orders_per_day.query('product_id == @item').sort_values('number_orders', ascending=False)
item_dow
```
![item_dow](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/item_dow.png?ref_type=heads)

 
To obtain a table with only those days of the week, when the product is bought most often, let's add another column, where we calculate the maximum values of the number of orders per day for each product:

```python
orders_per_day['day_with_most_frequency'] = orders_per_day.groupby('product_id')['number_orders'].transform('max')
orders_per_day.sort_values('day_with_most_frequency', ascending=False)

# remove unnecessary rows and columns

most_frequent_oreders = orders_per_day\
.query('number_orders == day_with_most_frequency')[['product_id', 'day_of_week', 'day_with_most_frequency']]
most_frequent_oreders.sort_values('day_with_most_frequency', ascending=False)
```

We get a table:

![Which day of the week the product is most frequently purchased](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/frequentest_orderes.png?ref_type=heads)

 
On some days of the week some product was ordered with the same high frequency. Such a product may have more than one row in the summary table. 

![Days with the same frequency of product ordering](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/thesame_freq.png?ref_type=heads)

### 4. How many purchases does each user make on average per week (by month) <a name="How_many_purchases"></a> 

Let me remind you that we agreed to consider that the user has made a purchase if he/she has paid for the order (there is a time of payment confirmation). So, the date of purchase is in the order_approved_at column.

 
For the merged table we will take order id and order_approved_at from orders. From the table with customers - customer_unique_id. Merge by customer_id:

```python
user_orders = orders[['order_id', 'customer_id', 'order_approved_at']]\
.merge(customers[['customer_id', 'customer_unique_id']], how='left', on='customer_id')

# Let's remove empty cells because an unpaid order is not a purchase 

user_orders = user_orders.dropna()
```
 
Next, we will create 2 columns in the new table: 
1) For the first one we will extract the month from the payment confirmation date
2) In the second one we will count the number of weeks in each month


```python
user_orders['order_approved_year_month'] = user_orders.order_approved_at.dt.to_period("M")
user_orders['weeks'] = round((user_orders.order_approved_year_month.dt.days_in_month / 7), 2)
# delete the incomplete first and last months
user_orders = user_orders.query('"2016-09" < order_approved_year_month < "2018-09"')

# group the data by customer and by month and calculate how many orders each customer made in a month 
purchases_per_week = user_orders.groupby(['customer_unique_id', 'order_approved_year_month', 'weeks'], as_index=False)\
.agg({'order_id':'count'})

# Divide the number of orders by the number of weeks
purchases_per_week['purchases_per_week'] = purchases_per_week.order_id / purchases_per_week.weeks

purchases_per_week.sort_values('purchases_per_week', ascending=False)
```
As a result, we get the table:

![Purchases per week by month](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/purchases_per_week.png?ref_type=heads)

![Purchases per week. Statistics](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/purchases_per_week.describe.png?ref_type=heads)

**75% of clients make 0.23 purchases per week or one purchase per month. The maximum is 1.35 purchases per week (6 purchases per month).**

### 5. The cohort analysis of users <a name="5_The_cohort_analysis_of_users"></a>
The notebook with a code - [User_cohort_analysis](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/User_cohort_analysis.ipynb?ref_type=heads).

**Basic Concepts:**   

1. Sign of cohort formation: month in which the customer paid for the first order.
2. Cohort size is a time interval equal to one month.
3. Reporting period: January - December 2017.
4. The key indicator is the Retention Rate. We calculate it through repeat purchases: how many percent of customers made purchases in the 2nd/3rd, etc month after the month from the 1st purchase.

First, let's calculate the dates of the first purchases for each customer

```python
purchases = customers[['customer_unique_id', 'customer_id']]\
.merge(orders[['order_id', 'order_approved_at', 'customer_id']], how='inner', on='customer_id')

# Let's leave the first purchases 
first_purchase = purchases.groupby('customer_unique_id', as_index=False).agg(first_purchase_date =('order_approved_at', 'min'))

# We will only leave out those customers whose first purchase is in 2017
first_purchase_2017 = first_purchase.query('"2016-12-31" < first_purchase_date < "2018-01-01"')
first_purchase_2017.sort_values('first_purchase_date')
```

Now let's merge the table with first purchases with the purchase table, add columns with months and count the number of customers and purchases:

```python
df = first_purchase_2017.merge(purchases,  how='inner',  on='customer_unique_id')

# let's keep the purchases in 2017 only
df = df.query('"2016-12-31" < order_approved_at < "2018-01-01"')

# add columns with months
df['start_month'] = df.first_purchase_date.dt.to_period("M")
df['purchase_month'] = df.order_approved_at.dt.month

# group and count the number of customers and purchases
cochorts = df.groupby(['start_month','purchase_month'], as_index=False)\
.agg({'order_id': 'count', 'customer_unique_id': 'count'})
cochorts = cochorts.rename(columns={'order_id': 'purchases', 'customer_unique_id': 'customers'})
cochorts = cochorts.sort_values(['start_month', 'purchase_month'])
```

Let's calculate the size of each group - the base for calculating retention. And then calculate the share of repeat purchases in the following months:

```python
cochorts['group_size'] = cochorts.groupby('start_month').customers.transform('first')
cochorts['retention'] = round(cochorts.customers/cochorts.group_size * 100, 2)
```

![Table Retention](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/tab_retention.png)


In essence, a cohort table is a summary table where the rows are the months of the first purchases, the columns are the period month numbers in order, and the cells at the intersection show the retention values. 

```python
cochorts_retention = cochorts.pivot(index= 'start_month', columns= 'purchase_month', values='retention').fillna(0)
```

![Cohort table](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/tab_cohorts.png)

Let's build a heat map.

```python
sns.heatmap(cochorts_retention, vmin=0, vmax=1, center= 0.37, annot=True, cmap='rocket', linecolor='white', linewidths=0.7, fmt=".1f")
```

![Heat map](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/heatmap.png)


### Cohort with the highest retention for the 3rd month for the period from January to December

To identify it, let's create a column with the difference between the first and the next purchase. To calculate it in months, we convert the difference into days and divide them by the average number of days in a month in 2017. Next, count the number of clients and create columns with group size and retention:

```python
df['cochort_period'] = round(((df.order_approved_at - df.first_purchase_date).dt.days)/30.4167, 0)

# count the number of clients
third_month_retention = df.groupby(['start_month', 'cochort_period'], as_index=False)\
.agg(customers =('customer_unique_id', 'count'))

# the base for calculating retention
third_month_retention['group_size'] = third_month_retention.groupby('start_month').customers.transform('first')

# share of repeat purchases in the following months
third_month_retention['retention'] = round(third_month_retention.customers/third_month_retention.group_size * 100, 2)

# We only keep the three-month difference
third_month_retention  = third_month_retention [third_month_retention.cochort_period == 3.0]
third_month_retention.sort_values('retention')
```

![Percentage of repeat purchases at the 3rd month](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/tab_cohort_3m.png)

###  Customers who made their first purchases in March 2017 have the highest retention rate for the 3rd month (0.48%).


### 6. RFM segmentation <a name="6_rfm_segmentation"></a>
**Metrics:**
   * R - время от последней покупки пользователя до текущей даты,
   * F - суммарное количество покупок у пользователя за всё время,
   * M - сумма покупок за всё время.

Покупка: всё также order_approved_at — время подтверждения оплаты заказа.

Возьмём нужные колонки из всех датасетов и создадим нужный датафрейм. Так как у нас есть в оплаченных отменённые заказы, уберём их, потому что они не принесли деньги.

```python
base = customers[['customer_unique_id', 'customer_id']]\
.merge(orders.query('order_status != "canceled"')[['customer_id', 'order_approved_at', 'order_id']], on='customer_id')\
.merge(items[['order_id', 'product_id', 'price']], how='left', on='order_id')

# удалим пустые ячейки - это неоплаченные заказы и те недоступные, которых даже нет в датасете items 
base = base.dropna()
```
Найдём разницу между датами покупок и текущей датой:

```python
NOW = pd.to_datetime(orders.order_purchase_timestamp.max() + dt.timedelta(days=1)) 
base['dif_days'] = round((NOW - base.order_approved_at) / np.timedelta64(1, 'D'), 0)
```

Вычислим интересующие нас метрики.

```python
rfm = base.groupby('customer_unique_id').agg({'dif_days': 'min', 'order_id': 'count', 'price': 'sum'}).reset_index()
rfm.rename(columns={'dif_days': 'recency', 'order_id': 'frequency', 'price': 'monetary'}, inplace=True)
rfm.sort_values('monetary', ascending=False)
```
![Таблица RFM](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/rfm_tab.png)

![RFM статистика](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/rfm_describe.png)

Помним из [предыдущего анализа](#Количество пользователей, совершивших покупку только один раз), что 93049 пользователей совершили покупку только один раз. Поэтому статданные не являются сюрпризом.

В течение последних 44 дней не было покупок вообще. Так и есть: последний оплаченный заказ был в 2018-09-03 17:40:06. И это странно. Возможно, прежде чем выводить rfm, стоило изучить и отладить данный факт. Но мне не кажется правильным подгонать датасет, убирая из него месяцы, если я не уверена, что причина в технической неполадке.

![Displot Recency](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/recency_bars.png)

![Displot Monetary](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/monetary_bars.png)

### Создаём кластеры
Предлагаю для кластеризации recency и monetary использовать квартили. А для frequency, т.к. 75% клиентов сделали по 1 покупке, разбить следующим образом:

1. 1
2. 2-10
3. 9-20
4. 20

```python
rfm.frequency.sort_values().unique()
quintiles = rfm[['recency', 'monetary']].quantile([.25, .5, .75]).to_dict()

#задаём функции для сегментов
def r_score(x):
    if x <= quintiles['recency'][.25]:
        return 4
    elif x <= quintiles['recency'][.5]:
        return 3
    elif x <= quintiles['recency'][.75]:
        return 2
    else:
        return 1

def m_score(x):
    if x <= quintiles['monetary'][.25]:
        return 1
    elif x <= quintiles['monetary'][.5]:
        return 2
    elif x <= quintiles['monetary'][.75]:
        return 3
    else:
        return 4 
    
def f_score(x):
    if x == 1:
        return 1
    elif x <= 10:
        return 2
    elif x <= 20:
        return 3
    else:
        return 4  



# добавляем сегменты в таблицу

rfm['R'] = rfm['recency'].apply(lambda x: r_score(x))
rfm['F'] = rfm['frequency'].apply(lambda x: f_score(x))
rfm['M'] = rfm['monetary'].apply(lambda x: m_score(x))

# Соединяем все 3 оценки в 1 колонку
rfm['rfm_score'] = rfm['R'].map(str) + rfm['F'].map(str) + rfm['M'].map(str)
rfm
```

![Таблица с колонкой сводной оценки](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/rfm_score.png)

### Описание сегментов
* champions: покупали недавно, покупают много, (он у нас 1 такой)))
* potential loyalists: покупали недавно, больше 1 раза,
* new customers: покупали недавно, 1 раз,
* loyal customers: покупают чаще других, но последняя покупка была не так близко,
* need attention: покупали больше 1 раза, но последняя покупка была не так близко,
* at risk: мы их теряем, купили достаточно давно, но больше 1 раза,
* hibernating: уснувшие, возможно, потерянные, купили всего 1 раз и очень давно

Добавим это описание в таблицу

```python
segt_map = {
    r'11': 'hibernating',
    r'[1-2][1-4]': 'at risk',
    r'[2-3]1': 'need attention',
    r'3[2-4]': 'loyal customers',
    r'41': 'new customers',
    r'4[2-3]': 'potential loyalists',
    r'44': 'champions'
}

rfm['Segment'] = rfm['R'].map(str) + rfm['F'].map(str)
rfm['Segment'] = rfm['Segment'].replace(segt_map, regex=True)
```

И вычислим долю каждого сегмента:

```python
rfm_share = rfm.groupby('Segment', as_index=False).agg(total =('customer_unique_id', 'count'))
rfm_share['share'] = rfm_share.total/rfm.customer_unique_id.nunique()*100
rfm_share   
```

![Таблица RFM_share](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/rfm_share.png)

![Сегменты RFM_share](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/segments_bars.png)

### Строим визуализации
```python
#plot the distribution of customers over R and F
fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(10, 4))

for i, p in enumerate(['R', 'F']):
    parameters = {'R':'Recency', 'F':'Frequency'}
    y = rfm[p].value_counts().sort_index()
    x = y.index
    ax = axes[i]
    bars = ax.bar(x, y, color='silver')
    ax.set_frame_on(False)
    ax.tick_params(left=False, labelleft=False, bottom=False)
    ax.set_title('Distribution of {}'.format(parameters[p]),
                fontsize=14)
    for bar in bars:
        value = bar.get_height()
        if value == y.max():
            bar.set_color('firebrick')
        ax.text(bar.get_x() + bar.get_width() / 2,
                value - 5,
                '{}\n({}%)'.format(int(value), int(value * 100 / y.sum())),
               ha='center',
               va='top',
               color='w')
plt.savefig("distribution_R_F.png", dpi=100, bbox_inches="tight")
plt.show()

# plot the distribution of M for RF score
fig, axes = plt.subplots(nrows=5, ncols=5,
                         sharex=False, sharey=True,
                         figsize=(10, 10))

r_range = range(1, 5)
f_range = range(1, 5)
for r in r_range:
    for f in f_range:
        y = rfm[(rfm['R'] == r) & (rfm['F'] == f)]['M'].value_counts().sort_index()
        x = y.index
        ax = axes[r - 1, f - 1]
        bars = ax.bar(x, y, color='silver')
        if r == 5:
            if f == 3:
                ax.set_xlabel('{}\nF'.format(f), va='top')
            else:
                ax.set_xlabel('{}\n'.format(f), va='top')
        if f == 1:
            if r == 3:
                ax.set_ylabel('R\n{}'.format(r))
            else:
                ax.set_ylabel(r)
        ax.set_frame_on(False)
        ax.tick_params(left=False, labelleft=False, bottom=False)
        ax.set_xticks(x)
        ax.set_xticklabels(x, fontsize=8)

        for bar in bars:
            value = bar.get_height()
            if value == y.max():
                bar.set_color('firebrick')
            ax.text(bar.get_x() + bar.get_width() / 2,
                    value,
                    int(value),
                    ha='center',
                    va='bottom',
                    color='k')
fig.suptitle('Distribution of M for each F and R',
             fontsize=14)
plt.tight_layout()
plt.savefig("distribution_M_for_RF.png", dpi=100, bbox_inches="tight")
plt.show()
```

![Распределение Recency и Frequency](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/distribution_R_F.png)

![Распределение Monetary для каждого R и F](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/distribution_M_for_RF.png)

```python
# count the number of customers in each segment
segments_counts = rfm['Segment'].value_counts().sort_values(ascending=True)

fig, ax = plt.subplots()

bars = ax.barh(range(len(segments_counts)),
              segments_counts,
              color='silver')
ax.set_frame_on(False)
ax.tick_params(left=False,
               bottom=False,
               labelbottom=False)
ax.set_yticks(range(len(segments_counts)))
ax.set_yticklabels(segments_counts.index)

for i, bar in enumerate(bars):
        value = bar.get_width()
        if segments_counts.index[i] in ['champions', 'loyal customers']:
            bar.set_color('firebrick')
        ax.text(value,
                bar.get_y() + bar.get_height()/2,
                '{:,} ({:}%)'.format(int(value),
                                   int(value*100/segments_counts.sum())),
                va='center',
                ha='left'
               )
plt.savefig("segments_counts.png", dpi=100, bbox_inches="tight")
plt.show()
```

![Сегменты](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/segments_counts.png)

## Выводы
* Достаточно равномерное распределение параметра Recency
* 87% клиентов купили 1 раз и 12% - 2 раза.
* У нас есть 1 чемпион с параметрами 444
* 22% клиентов купили 1 раз и больше года назад
* 27,5% покупали последний раз достаточно давно (около года назад и больше), но больше 1 раза
* 22% купили 1 раз где-то полгода - 9 месяцев назад.
* Лояльных клиентов и потенциально лояльных очень мало (по 3%)

## Roadmap
Исходя из этого анализа, я бы порекомендовала компании: 
1. Изучить, по каким причинам пользователи отменяют заказы и можно ли уменьшить количество отмен.
2. Пересмотреть расчёт обещанных дат доставки заказов до клиента. Так как 75 % задержек не превышает 12 дней.
Стоит посмотреть, что за продавцы, почему у них такие задержки и можно ли их сократить. И если нельзя, то сделать разные алгоритмы расчёта дат доставки для разных продавцов или разных товаров. И тогда количество недоставленных заказов уменьшиться как минимум процентов на 30.



## My contacts

Если у вас появились вопросы или предложения по сотрудничеству, пожалуйста, напишите мне

* в Телеграм @Natalie_l_Swan
* или на почту tasha.l.swan@gmail.com


 