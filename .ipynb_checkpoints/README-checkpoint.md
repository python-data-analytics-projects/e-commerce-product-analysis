# E-Commerce Product Analysis
Проект представляет из себя исследовательский анализ данных,  выполненный по заданию продакт-менеджера e-commerce компании. 

## Цель 
Проанализировать совершенные покупки и ответить на следующие вопросы:

1. Количество пользователей, совершивших покупку только один раз.
2. Сколько заказов в месяц в среднем не доставляется по разным причинам? Вывести детализацию по причинам.
3. По каждому товару определить, в какой день недели товар покупается чаще всего.
4. Сколько у каждого из пользователей в среднем покупок в неделю (по месяцам)?
5. Провести когортный анализ пользователей. Выявить когорту с самым высоким retention на 3й месяц за период с января по декабрь.
6. Построить RFM-сегментацию пользователей с метриками:
   * R - время от последней покупки пользователя до текущей даты,
   * F - суммарное количество покупок у пользователя за всё время,
   * M - сумма покупок за всё время.
  
## Datasets 

Для анализа использовалось 3 датасета:
1. [Таблица с уникальными идентификаторами пользователей](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/olist_customers_dataset.csv?ref_type=heads) содержит следующие колонки:

* customer_id — позаказный идентификатор пользователя 
* customer_unique_id —  уникальный идентификатор пользователя  (аналог номера паспорта) 
* customer_zip_code_prefix —  почтовый индекс пользователя 
* customer_city —  город доставки пользователя 
* customer_state —  штат доставки пользователя 

2.  [Таблица заказов](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/olist_orders_dataset.csv?ref_type=heads) отражает данные о заказах:

* order_id —  уникальный идентификатор заказа (номер чека) 
* customer_id —  позаказный идентификатор пользователя 
* order_status —  статус заказа. Уникальные статусы заказов: 
  * created —  создан 
  * approved —  подтверждён
  * invoiced —  выставлен счёт
  * processing —  в процессе сборки заказа
  * shipped —  отгружен со склада
  * delivered —  доставлен пользователю
  * unavailable —  недоступен
  * canceled —  отменён
    
* order_purchase_timestamp —  время создания заказа 
* order_approved_at —  время подтверждения оплаты заказа 
* order_delivered_carrier_date —  время передачи заказа в логистическую службу 
* order_delivered_customer_date —  время доставки заказа 
* order_estimated_delivery_date —  обещанная дата доставки

3. Файл [olist_order_items_dataset.csv](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/olist_order_items_dataset.csv?ref_type=heads) содержит информацию о товарных позициях, входящих в заказы: 

* order_id —  уникальный идентификатор заказа (номер чека) 
* order_item_id —  идентификатор товара внутри одного заказа 
* product_id —  ид товара (аналог штрихкода) 
* seller_id — ид производителя товара 
* shipping_limit_date —  максимальная дата доставки продавцом для передачи заказа партнеру по логистике 
* price —  цена за единицу товара 
* freight_value —  вес товара 

 [Пример структуры данных в третьем датасэте.](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/%D0%9F%D1%80%D0%B8%D0%BC%D0%B5%D1%80%20%D1%81%D1%82%D1%80%D1%83%D0%BA%D1%82%D1%83%D1%80%D1%8B%20%D0%B4%D0%B0%D0%BD%D0%BD%D1%8B%D1%85.png?ref_type=heads) 

 Одна строчка - 1 товар. Если товар заказан в количестве, например, 3 шт., то будет 3 строчки с одним id заказа и одним и тем же id товара.

Все таблицы лежат в этом репозитории. 

 ## Стек

 * Python c библиотеками:
    * Pandas
    * numpy
    * seaborn
    * matplotlib

* EDA
* Когортный анализ
* RFM анализ

## Результат

Для удобства изучения разделила проект на 3 файла/тетради с кодом:

* В [первом файле](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/Project_e-commerce_by_Natalie_Lebedeva(Swan).ipynb?ref_type=heads) проведён предварительный анализ данных (EDA) и даны ответы на первые 4 вопроса.
* [Второй](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/User_cohort_analysis.ipynb?ref_type=heads) содержит код для когортного анализа
* В [третьем](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/rfm_segmentation.ipynb?ref_type=heads) вы найдёте RFM анализ

## EDA
Подробный анализ смотрите в [первой тетради с кодом](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/Project_e-commerce_by_Natalie_Lebedeva(Swan).ipynb?ref_type=heads).
Здесь вынесу только основные пункты. 

Все даты перевела в формат datetime. 
* Первая дата в данных - дата формирования заказа - 4 сент 2016. 
* Последняя дата - 17 окт 2018 г. - дата формирования заказа и дата доставки клиенту.
* Последняя расчетная дата доставки - 11 дек 2018г.

Поэтому в рассуждениях буду исходить из того, что данные были выгружены 17 окт 2018 г., и приму эту дату за текущую.

Заказы делались из 27 штатов и из 4119 городов Бразилии, где всего 26 штатов и 1 округ.

Пустые ячейки только в трёх колонках одной из 3х таблиц. И выглядят они достаточно логично. Скорее всего, они означают, что просто не было тех операций с заказом, где ячейки не заполнены. У каждого ордера есть дата его создания и дата обещанной доставки. Видимо, 160 заказов не были оплачены, 1783 не были доставлены в логистическую компанию и 2965 не были доставлены до клиента. Хотя не исключены технические сбои. 

![Количество пустых ячеек в olist_order_items_dataset.csv](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/isna.png?ref_type=heads)

В целом база достаточно чистая, но есть есть пара моментов.

### Вопросы по данным 
В таблице есть статус заказа, а есть даты, в которые можно посмотреть, что происходило с заказом. Как я поняла, последовательность такая: 
1. заказ фомируется - статус created,
2. затем подтверждается заказчиком - статус approved,
3. затем выставляется счёт - статус invoiced,
4. и только потом происходит оплата и мы видим дату её подтверждения, а заказ уходит в работу со статусом processing.
  
В данных есть всего 2 заказа со статусом approved, оба оплачены. Почему эти заказы так и не были доставлены до 11 дек 2018г (при обещанной дате доставки в 2017г), несмотря на то, что заказ подтверждён, оплачен и не отменён? Технически получается, что счёт оплатили до того, как он был выставлен, ведь оплаченный заказ должен иметь статус как минимум invoiced или processing. Похоже на баг.

![Недоставленные оплаченные заказы со статусом approved](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/approved.png?ref_type=heads)


Есть и такие аномалии: заказ может иметь статус "доставлен", но при этом не иметь даты оплаты (14 заказов), даты доставки в логистическую компанию (2 заказа) и даже даты доставки до клиента (8 заказов). Но это всё единичные случаи, выглядят сбоем.

![Баги со статусом delivered](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/delivered.png?ref_type=heads)

Более серьёзные вопросы вызывают оплаченные заказы, которые должны были быть доставлены (иногда задолго) до того дня, когда выгрузили данные в csv - 17 окт 2018 г. - но почему-то зависли каждый на своих стадиях. Но эту проблему рассмотрим при ответе на 2й вопрос проекта.  


## Основные выводы

### 1. Количество пользователей, совершивших покупку только один раз
Предлагаю считать покупкой созданный и оплаченный заказ (есть дата подтверждения оплаты). Подробное исследование этой темы можете посмотреть в первой тетради с кодом. 

Для ответа на этот вопрос создала новый dataframe, взяв колонку "время подтверждения оплаты заказа" (order_approved_at) из таблицы заказов и объединив её с id пользователей из таблицы с уникальными идентификаторами пользователей, при этом удалила пустые ячейки = неоплаченные заказы:

```python
paid_orders = orders[['customer_id', 'order_approved_at']]\
.merge(customers[['customer_id', 'customer_unique_id']], how='inner', on='customer_id')\
.dropna()
```

Далее посчитала, сколько заказов приходится на каждого пользователя и посчитала тех, у кого по 1 заказу

```python
num_orders = pd.DataFrame(paid_orders.groupby('customer_unique_id', as_index=False)['customer_id'].count())
customers_with_1_purchase = num_orders.query('customer_id == 1')['customer_unique_id'].count()

print(customers_with_1_purchase, 'пользователей совершили покупку только один раз')
```

### 93049 (из 96096 уникальных) пользователей совершили покупку 1 раз. 


### 2. Сколько заказов в месяц в среднем не доставляется по разным причинам? Вывести детализацию по причинам.
**Что считать недоставленным товаром**

Для начала посмотрим, есть ли заказы, доставленные после обещанной даты.

```python
delays = orders.query('order_delivered_customer_date > order_estimated_delivery_date')
delays.order_id.nunique()
```
7827 заказов доставлены с опозданием. В разрезе по месяцам они могут считаться недоставленными. Соответственно, определять, доставлен заказ или нет, буду в месяц, на который назначена обещанная дата доставки.

```python
# Отфильтруем заказы, доставленные после обещанной даты и с пустыми ячейками в колонке order_delivered_customer_date
undelivered_orders = orders\
.query('order_delivered_customer_date > order_estimated_delivery_date or order_delivered_customer_date.isna()')

# Создадим новую колонку, чтобы посмотреть, сколько дней составляет задержка заказов
undelivered_orders['delay_days'] \
= (undelivered_orders.order_delivered_customer_date - undelivered_orders.order_estimated_delivery_date)/ np.timedelta64(1, 'D')
undelivered_orders.describe()
```

В среднем задержка доставки около 10 дней. Половину заказов доставляют не больше, чем через 6 дней после обещанной даты. 75 % задержек не превышает 12 дней. Но иногда и по полгода люди ждут свои заказы. 

![Задержка доставки, дни](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/boxplot_delay.png?ref_type=heads)

Вопрос в том, что считать просто задержкой, а что - недоставленным товаром. 
Допустим, мы смотрим данные в первый день каждого месяца. Какие-то заказы будут доставлены в предыдущем месяце вовремя, какие-то с задержкой, а какие-то на не будут иметь дату доставки на день анализа. Возможно, они будут доставлены в позже, но обычно человек не может знать наверняка, что произойдёт в следующем месяце, поэтому логично считать товары, не имеющие отметку о доставке в текущем месяце (при обещанной дате в этом месяце), недоставленными.    

**Заказ считается недоставленным, если он не был доставлен в обещанном месяце**. 
И введём дополнительную "поблажку": если задержка не больше суток, то заказ будем считать доставленным (на случай, если обещанная дата доставки - последний день месяца, а мы анализируем в первый день следующего). 

```python
# отфильтруем заказы, которые доставили с задержкой меньше чем 2 суток 

undelivered_orders = undelivered_orders\
.query('delay_days >= 2 or order_delivered_customer_date.isna()') 

# теперь оставим только те заказы, которые не доставили в месяц обещанной даты:
undelivered_orders = undelivered_orders\
.query('(order_delivered_customer_date.dt.to_period("M") > estimated_delivery_year_month) or (order_delivered_customer_date.isna())') 
undelivered_orders.order_id.nunique()
```
**Итого получилось 4938 недоставленных заказа, если считать недоставленными заказы, полученные позже месяца, в котором заказ должен был быть доставлен.**  
 Сгруппируем их по месяцам и посчитаем среднее

 ```python
undelivered_orders.groupby(['estimated_delivery_year_month']).agg({'order_id':'count'}).mean()
```

### Около 190 заказов в месяц в среднем не доставляется по разным причинам.
Посмотрим, как распределены статусы среди недоставленных заказов

```python
data = undelivered_orders.groupby('order_status', as_index=False)[['order_id']].count()

#объединим статусы approved и created  в other
other = ["approved", "created"]
a = data.query('order_status in @other')['order_id'].agg("sum")
new_row = pd.Series({'order_status': 'other', 'order_id': a})
data_1 = pd.concat([data, pd.DataFrame([new_row])], ignore_index=True).sort_values(['order_id']).reset_index(drop=True)
data_1.drop(data_1.index[[0,1]], inplace=True)
data_1
```
![Таблица Статусы недоставленных заказов](statuses_tab)

![Статусы недоставленных заказов](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/Project_e-commerce_by_Natalie_Lebedeva(Swan).ipynb?ref_type=heads)

* 40% были в итоге доставлены, но позже обещанного срока, даже не в обещанный месяц*.
*Если обещанная дата приходится на конец месяца и задержка больше суток, то заказ попадает в список недоставленных.

* 13% были отменены
* 12% имеют статус "недоступен" 
* 22% были переданы в логистическую службу и застряли там
* 12% висят в статусах created, approved, invoiced и processing

Если последние статусы более или менее понятны, то что или кто недоступен, не очень понятно. В таблице items нашлось только 6 заказов (из 609) со статусом unavailable. Причём дата доставки в логистику есть, и она явно не на 2 года спланирована. Но почему-то эти товары так и не были отправлены продавцом. 

Куда пропали остальные 603 заказа? Может быть, недоступны стали продавцы, которые продавали заказанный товар?

### 3. По каждому товару определить, в какой день недели товар покупается чаще всего.

Под "покупается" имеем в виду день формирования заказа. 

Для решения возьмём время создания заказа из таблицы заказов и объединим с id продукта из таблицы items. Из времени заказа выделим день недели. И сгруппируем по product_id и дням недели.

```python
ordered_items = orders[['order_id', 'order_purchase_timestamp']]\
.merge(items[['order_id', 'product_id']], how='left', on='order_id')
ordered_items['day_of_week'] = ordered_items.order_purchase_timestamp.dt.day_name()
orders_per_day = ordered_items.groupby(['product_id', 'day_of_week'], as_index=False)\
.agg(number_orders = ('order_id', 'count'))
orders_per_day.sort_values('number_orders', ascending=False)
```

В этот датафрейм можно подставить интересующий нас товар и посмотреть недельный расклад по нему:

```python
item = '422879e10f46682990de24d770e7f83d'
item_dow = orders_per_day.query('product_id == @item').sort_values('number_orders', ascending=False)
item_dow
```
![item_dow](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/item_dow.png?ref_type=heads)

Для получения таблицы только с теми днями недели, когда товар покупается чаще всего, добавим ещё одну колонку, где будут максимальные значения кол-ва заказов в день в разрезе по товарам.

```python
orders_per_day['day_with_most_frequency'] = orders_per_day.groupby('product_id')['number_orders'].transform('max')
orders_per_day.sort_values('day_with_most_frequency', ascending=False)

# Уберём лишние строки и колонки

most_frequent_oreders = orders_per_day\
.query('number_orders == day_with_most_frequency')[['product_id', 'day_of_week', 'day_with_most_frequency']]
most_frequent_oreders.sort_values('day_with_most_frequency', ascending=False)
```

И получим такую табличку:

![В какой день недели товар покупается чаще всего](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/frequentest_orderes.png?ref_type=heads)

В некоторые дни недели товар заказывали с одинаково высокой частотой. Такой товар может иметь больше одной строки в итоговой таблице. 

![Дни с одинаковой частотой заказа товара](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/thesame_freq.png?ref_type=heads)

### 4. Сколько у каждого из пользователей в среднем покупок в неделю (по месяцам)?

Как мы договорились раньше, пользователь покупку совершил, если оплатил заказ (есть время подтверждения оплаты). Значит, дата покупки находится в колонке order_approved_at.

Для объединённой таблицы из orders возьмём id заказа и order_approved_at. Из таблицы с пользователями - уникальный идентификатор пользователя.Объединим по позаказному идентификатору пользователя:

```python
user_orders = orders[['order_id', 'customer_id', 'order_approved_at']]\
.merge(customers[['customer_id', 'customer_unique_id']], how='left', on='customer_id')

# Удалим пустые ячейки, потому что неоплаченный заказ - это не покупка 

user_orders = user_orders.dropna()
```
Далее в новой таблице создадим 2 колонки: 
1) Для первой извлечём месяц из даты потверждения платежа
2) Во второй посчитаем количество недель в каждом месяце

```python
user_orders['order_approved_year_month'] = user_orders.order_approved_at.dt.to_period("M")
user_orders['weeks'] = round((user_orders.order_approved_year_month.dt.days_in_month / 7), 2)
# удалим неполные крайние месяцы
user_orders = user_orders.query('"2016-09" < order_approved_year_month < "2018-09"')

# Сгруппируем по пользователям и по месяцам и посчитаем кол-во сделанных заказов в месяц 
purchases_per_week = user_orders.groupby(['customer_unique_id', 'order_approved_year_month', 'weeks'], as_index=False)\
.agg({'order_id':'count'})

# Разделим кол-во заказов на кол-во недель
purchases_per_week['purchases_per_week'] = purchases_per_week.order_id / purchases_per_week.weeks

purchases_per_week.sort_values('purchases_per_week', ascending=False)
```
В итоге получаем таблицу:

![Количество покупок в неделю по месяцам](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/purchases_per_week.png?ref_type=heads)

![Количество покупок в неделю. Статистика](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/purchases_per_week.describe.png?ref_type=heads)

**75% клиентов совершают 0,23 покупки в неделю или одну покупку в месяц. Максимум - 1,35 покупок в неделю (6 покупок в месяц).**

### 5.  Когортный анализ пользователей. 
Тетрадь с кодом для расчёта - [User_cohort_analysis](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/User_cohort_analysis.ipynb?ref_type=heads).

**Основные понятия:**

1. Признак формирования когорты: месяц, в котором оплачен первый заказ (раз говорим о покупках и определили ранее, что покупка - это оплаченный заказ).
2. Размер когорты — временной интервал: 1 месяц.
3. Отчетный период: с января по декабрь 2017г.
4. Анализируемый ключевой показатель: Retention Rate. Коэффициент удержания рассчитываем через повторные покупки: сколько процентов пользователей сделают покупки во 2м месяце после месяца с 1й покупки, сколько процентов - в 3м и т.д.

Для начала рассчитаем даты первых покупок для каждого клиента

```python
# Из таблицы customers нам нужен только customer_unique_id. Замёрджим его к таблице orders

purchases = customers[['customer_unique_id', 'customer_id']]\
.merge(orders[['order_id', 'order_approved_at', 'customer_id']], how='inner', on='customer_id')

# Оставим первые покупки 
first_purchase = purchases.groupby('customer_unique_id', as_index=False).agg(first_purchase_date =('order_approved_at', 'min'))

# Оставим только тех клиентов, у кого первая покупка приходится на 2017 год.
first_purchase_2017 = first_purchase.query('"2016-12-31" < first_purchase_date < "2018-01-01"')
first_purchase_2017.sort_values('first_purchase_date')
```

Теперь бъединим таблицу с первыми покупками с таблицей покупок, добавим колонки с месяцами и посчитаем количество клиентов и покупок:

```python
df = first_purchase_2017.merge(purchases,  how='inner',  on='customer_unique_id')

# оставим покупки только 2017 г
df = df.query('"2016-12-31" < order_approved_at < "2018-01-01"')

# Добавим колонки с месяцами
df['start_month'] = df.first_purchase_date.dt.to_period("M")
df['purchase_month'] = df.order_approved_at.dt.month

# Группируем и считаем кол-во клиентов и покупок
cochorts = df.groupby(['start_month','purchase_month'], as_index=False)\
.agg({'order_id': 'count', 'customer_unique_id': 'count'})
cochorts = cochorts.rename(columns={'order_id': 'purchases', 'customer_unique_id': 'customers'})
cochorts = cochorts.sort_values(['start_month', 'purchase_month'])
```

Посчитаем размер каждой группы - базу для вычисления retention. И затем вычислим долю повторных покупок в следующих месяцах:

```python
cochorts['group_size'] = cochorts.groupby('start_month').customers.transform('first')
cochorts['retention'] = round(cochorts.customers/cochorts.group_size * 100, 2)
```

Получилась такая табличка

![Таблица Retention](retention_tab)


Когортная таблица - это, по существу, сводная таблица, где в строках - месяцы первых покупок, в колонках - номера месяцев периода по порядку, а в ячейках на пересечении - значения колонки retention. 

```python
cochorts_retention = cochorts.pivot(index= 'start_month', columns= 'purchase_month', values='retention').fillna(0)
```

![Когортная таблица](tab_cohorts)

Построим тепловую карту.

```python
sns.heatmap(cochorts_retention, vmin=0, vmax=1, center= 0.37, annot=True, cmap='rocket', linecolor='white', linewidths=0.7, fmt=".1f")
```

![Тепловая карта](heatmap)


### Когорта с самым высоким retention на 3й месяц за период с января по декабрь.

Для её выявления создадим столбец с разницей между первой и следующей покупкой. Чтобы посчитать её в месяцах, переведём разницу в дни и разделим их на среднее количество дней в месяце в 2017 г. Далее посчитаем количество клиентов и создадим колонки с размером групп и retention:

```python
df['cochort_period'] = round(((df.order_approved_at - df.first_purchase_date).dt.days)/30.4167, 0)

# Группируем  и считаем кол-во клиентов 
third_month_retention = df.groupby(['start_month', 'cochort_period'], as_index=False)\
.agg(customers =('customer_unique_id', 'count'))

# База для вычисления retention
third_month_retention['group_size'] = third_month_retention.groupby('start_month').customers.transform('first')

# Считаем долю повторных покупок в следующих месяцах
third_month_retention['retention'] = round(third_month_retention.customers/third_month_retention.group_size * 100, 2)

# Оставляем только разницу в 3 месяца
third_month_retention  = third_month_retention [third_month_retention.cochort_period == 3.0]
third_month_retention.sort_values('retention')
```

![Процент повторных покупок на 3й месяц](tab_cohort_3M)

### Самый высокий уровень повторных покупок на 3й месяц (0,48%) показали клиенты, сделавшие первые покупки в марте 2017г.


### 6. RFM-сегментация пользователей 
Метрики:
   * R - время от последней покупки пользователя до текущей даты,
   * F - суммарное количество покупок у пользователя за всё время,
   * M - сумма покупок за всё время.
  
## Roadmap
Исходя из этого анализа, я бы порекомендовала компании: 
1. Изучить, по каким причинам пользователи отменяют заказы и можно ли уменьшить количество отмен.
2. Пересмотреть расчёт обещанных дат доставки заказов до клиента. Так как 75 % задержек не превышает 12 дней.
Стоит посмотреть, что за продавцы, почему у них такие задержки и можно ли их сократить. И если нельзя, то сделать разные алгоритмы расчёта дат доставки для разных продавцов или разных товаров. И тогда количество недоставленных заказов уменьшиться как минимум процентов на 30.



## My contacts

Если у вас появились вопросы или предложения по сотрудничеству, пожалуйста, напишите мне

в Телеграм @Natalie_l_Swan
или на почту tasha.l.swan@gmail.com


 